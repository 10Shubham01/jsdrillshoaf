function flatten(elements) {
  if (Array.isArray(elements)) {
    let flattenArray = [];
    for (let index = 0; index < elements.length; index++) {
      Array.isArray(elements[index])
        ? (flattenArray = flattenArray.concat(flatten(elements[index])))
        : flattenArray.push(elements[index]);
    }
    return flattenArray;
  }
}
module.exports = flatten;
