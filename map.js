function map(elements, cb) {
  if (cb && Array.isArray(elements)) {
    let mappedArray = [];
    for (let index = 0; index < elements.length; index++) {
      mappedArray.push(cb(elements[index]));
    }
    return mappedArray;
  }
}
module.exports = map;
