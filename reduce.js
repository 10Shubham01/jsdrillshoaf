function reduce(elements, cb, startingValue) {
  if (cb && Array.isArray(elements) && startingValue) {
    let reducedValue = 0;
    for (let index = 0; index < elements.length; index++) {
      reducedValue = cb(elements[index]);
    }
    return reducedValue + startingValue;
  }
}
module.exports = reduce;
