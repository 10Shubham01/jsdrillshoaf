function filter(elements, cb) {
  if (cb && Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      if (cb(elements[index])) {
        return elements[index];
      }
    }
  }
}
module.exports = filter;
