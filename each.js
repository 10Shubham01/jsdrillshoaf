function each(elements, cb) {
  if (cb && Array.isArray(elements)) {
    if (Array.isArray(elements)) {
      for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
      }
    }
  }
  return;
}
module.exports = each;
