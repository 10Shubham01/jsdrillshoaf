const items = [1, 2, 3, 4, 5, 5];
const find = require("../find");
try {
  const cbFunction = (item) => {
    return item == 5 ? true : false;
  };

  console.log(find(items, cbFunction));
} catch (e) {
  console.log(e.message);
}
