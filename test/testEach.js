const items = [1, 2, 3, 4, 5, 5];
const each = require("../each");
try {
  const cbFunction = (item, index) => {
    console.log(`${item} present at ${index} index`);
  };
  console.log(each(items, cbFunction));
} catch (e) {
  console.log(e.message);
}
