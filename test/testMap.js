const items = [1, 2, 3, 4, 5, 5];
const map = require("../map");
try {
  const cbFunction = (item) => {
    return item * 2;
  };
  console.log(map(items, cbFunction));
} catch (e) {
  console.log(e.message);
}
