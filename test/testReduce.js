const items = [1, 2, 3, 4, 5, 5];
const reduce = require("../reduce");
try {
  let sum = 0;
  const cbFunction = (item) => {
    sum += item;
    return sum;
  };
  console.log(reduce(items, cbFunction, 5));
} catch (e) {
  console.log(e.message);
}
