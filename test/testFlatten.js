const nestedArray = [1, [2], [[3]], [[[4]]]];
const flatten = require("../flatten");
try {
  console.log(flatten(nestedArray));
} catch (e) {
  console.log(e.message);
}
